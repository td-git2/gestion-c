# Projet 4 : Gestionnaire de Contacts

## Objectif général du projet : Le but de ce projet est de développer un gestionnaire de contacts en Python en mettant en œuvre les principes de Git pour une collaboration efficace. Chaque membre du groupe sera responsable du développement d'une fonctionnalité spécifique.

## Consignes Etudiant 1
### Créez une nouvelle branche à partir de la branche principale, nommée "ajout_contacts".
![Q1.1](Screens/branche_ajout.jpg)
### Implémentez la logique pour permettre l'ajout de nouveaux contacts
![Q1.2](Screens/branche_ajout2.jpg)

## Consignes Etudiant 2
### Créez une nouvelle branche à partir de la branche principale, nommée "suppression_contacts".
![Q2.1](Screens/nouvelle branche supp.png)
### Implémentez la logique pour supprimer des contacts existants
![Q2.1](Screens/commit supp.png)

## Consignes Etudiant 3
### Créez une nouvelle branche à partir de la branche principale, nommée "affichage_contacts".
![Q3.1](Screens/branche_affichage.jpg)
### Implémentez la logique pour afficher la liste des contacts.
[Q3.2](Screens/commit_affichage.jpg)

## Consignes Etudiant 4
### Créez une nouvelle branche à partir de la branche principale, nommée "integration_contacts".
![Q4](Screens/integration_contacts.jpg)

## Chef de groupe
### Fusionner toutes les branches dans une nouvelle branche
![Q5](Screens/integration_contacts_finale.jpg)
